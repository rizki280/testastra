import { Component, OnInit } from '@angular/core';
import {EmployeeService} from './employee.service'
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit{
  constructor(private employeService: EmployeeService){}
    baseURL = 'https://dummy.restapiexample.com/api/v1/'
    employeeData:any ;
    detailEmployeeData: any;

  title = 'test';
  id: any;
  ngOnInit(){
    
    // this.employeeData = this.employeService.callAPIV2().subscribe(res => {
    //   // console.log(res);
    //   return res;
    // });
    this.getAllEmploye();
     console.log(this.employeeData);
    
  }

  getAllEmploye(){
    this.employeService.getAllEmployee().subscribe(res => {
      console.log(res);
      this.employeeData = res.data;
    })
    console.log(this.employeeData);
  }

  getEmployee(id: any){
    this.employeService.getEmployee(id).subscribe(res => {
      this.detailEmployeeData = [res.data];
    })
    console.log(this.detailEmployeeData);
    
  }

  deleteEmployee(id: any){
    this.employeService.deleteEmployee(id).subscribe(res => {
      console.log(res);
      
    })
    // console.log(this.detailEmployeeData);
    
  }

  changeId(id: any){
    this.id = id
  }
  
  
}
